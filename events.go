package swayipc

import (
	"encoding/json"
)

type EventType uint32

const (
	WorkspaceEventType       EventType = 0x80000000
	ModeEventType            EventType = 0x80000002
	WindowEventType          EventType = 0x80000003
	BarconfigUpdateEventType EventType = 0x80000004
	BindingEventType         EventType = 0x80000005
	ShutdownEventType        EventType = 0x80000006
	TickEventType            EventType = 0x80000007
	BarStateUpdateEventType  EventType = 0x80000014
	InputEventType           EventType = 0x80000015
)

func (t EventType) String() string {
	switch t {
	case WorkspaceEventType:
		return "workspace"
	case ModeEventType:
		return "mode"
	case WindowEventType:
		return "window"
	case BarconfigUpdateEventType:
		return "barconfig_update"
	case BindingEventType:
		return "binding"
	case ShutdownEventType:
		return "shutdown"
	case TickEventType:
		return "tick"
	case BarStateUpdateEventType:
		return "bar_state_update"
	case InputEventType:
		return "input"
	default:
		return ""
	}
}

type Event interface {
	EventType() EventType
}

type EventHandler interface {
	HandleEvent(e Event)
}

type WorkspaceEvent struct {
	Change  string    `json:"change"`
	Current Workspace `json:"current"`
	Old     Workspace `json:"old"`
}

func (e *WorkspaceEvent) EventType() EventType { return WorkspaceEventType }

type ModeEvent struct {
	Change      string `json:"change"`
	PangoMarkup bool   `json:"pango_markup"`
}

func (e *ModeEvent) EventType() EventType { return ModeEventType }

type WindowEvent struct {
	Change    string `json:"change"`
	Container Node   `json:"container"`
}

func (e *WindowEvent) EventType() EventType { return WindowEventType }

func (c *Conn) RegisterEventHandler(ev EventHandler) {
	c.eventHandler = ev
}

func (c *Conn) emitEvent(m *message) {
	cons := _events[EventType(m.Header.Type)]
	if cons == nil {
		return
	}
	ev := cons()
	err := json.Unmarshal(m.Payload, ev)
	if err != nil {
		return
	}
	c.eventHandler.HandleEvent(ev)
}

type ptrIsEvent[T any] interface {
	Event
	*T
}

func cons[T any, P ptrIsEvent[T]]() Event {
	var ptr P = new(T)
	return ptr
}

var _events = map[EventType]func() Event{
	WorkspaceEventType: cons[WorkspaceEvent],
	ModeEventType:      cons[ModeEvent],
	WindowEventType:    cons[WindowEvent],
	/*
		barconfigUpdateEvent: cons[BarconfigUpdateEvent],
		bindingEvent:         cons[BindingEvent],
		shutdownEvent:        cons[ShutdownEvent],
		tickEvent:            cons[TickEvent],
		barStateUpdateEvent:  cons[BarStateUpdateEvent],
		inputEvent:           cons[InputEvent],
	*/
}

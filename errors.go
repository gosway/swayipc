package swayipc

import (
	"errors"
	"fmt"
)

type unexpectedMsgType int

func (e unexpectedMsgType) Error() string {
	return fmt.Sprintf("unexpected message type: %d", e)
}

var unexpectedMsg = errors.New("unexpected message")

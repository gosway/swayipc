package swayipc

import (
	"context"
	"testing"
)

func TestGetVersion(t *testing.T) {
	ctx := context.Background()
	conn, err := Connect(ctx)
	requireNoErr(t, err)
	ver, err := conn.GetVersion()
	assertNoErr(t, err)
	t.Log(ver)
}

func assertNoErr(t *testing.T, err error) {
	t.Helper()
	if err != nil {
		t.Errorf("Expected no error; got: %s", err)
	}
}
func requireNoErr(t *testing.T, err error) {
	t.Helper()
	if err != nil {
		t.Fatalf("Expected no error; got: %s", err)
	}
}

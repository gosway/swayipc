package swayipc

import (
	"context"
	"net"
	"os"
	"os/exec"
	"strings"
	"sync"
)

type Conn struct {
	conn         *net.UnixConn
	m            sync.Mutex
	calls        []call
	eventHandler EventHandler
	err          error
}

func Connect(ctx context.Context) (*Conn, error) {
	var (
		d   net.Dialer
		err error

		sock = os.Getenv("SWAYSOCK")
	)
	if sock == "" {
		sock, err = defaultSwaySock(ctx)
		if err != nil {
			return nil, err
		}
	}
	conn, err := d.DialContext(ctx, "unix", sock)
	if err != nil {
		return nil, err
	}
	c := &Conn{conn: conn.(*net.UnixConn)}
	go c.readMessages()
	return c, nil
}

func defaultSwaySock(ctx context.Context) (string, error) {
	cmd := exec.CommandContext(ctx, "sway", "--get-socketpath")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(out)), nil
}

package swayipc

import (
	"encoding/binary"
	"encoding/json"
)

type call struct {
	typ      MessageType
	response any
	err      chan error
}

func (c *Conn) readMessage(m *message) error {
	err := binary.Read(c.conn, binary.LittleEndian, &m.Header)
	if err != nil {
		return err
	}
	m.Payload = append(m.Payload[:0], make([]byte, m.Header.Len)...)
	err = binary.Read(c.conn, binary.LittleEndian, &m.Payload)
	if err != nil {
		return err
	}
	return nil
}

func (c *Conn) readMessages() {
	var (
		msg  message
		err  error
		call call
	)
	for {
		err = c.readMessage(&msg)
		if err != nil {
			c.err = err
			return
		}
		if (msg.Header.Type >> 31) == 1 {
			if c.eventHandler != nil {
				c.emitEvent(&msg)
			}
			continue
		}
		c.m.Lock()
		if len(c.calls) == 0 {
			c.err = unexpectedMsg
			c.m.Unlock()
			return
		} else if c.calls[0].typ != msg.Header.Type {
			c.err = unexpectedMsgType(msg.Header.Type)
			for _, call = range c.calls {
				call.err <- c.err
			}
			c.calls = c.calls[:0]
			c.m.Unlock()
			return
		}
		call = c.calls[0]
		c.calls = c.calls[1:]
		c.m.Unlock()
		err = json.Unmarshal(msg.Payload, call.response)
		call.err <- err
	}
}

func (c *Conn) writeMessage(m *message) error {
	err := binary.Write(c.conn, binary.LittleEndian, m.Header)
	if err != nil {
		return err
	}
	err = binary.Write(c.conn, binary.LittleEndian, m.Payload[:m.Header.Len])
	if err != nil {
		return err
	}
	return nil
}

func (c *Conn) roundTrip(t MessageType, req, resp any) error {
	c.m.Lock()
	if c.err != nil {
		c.m.Unlock()
		return c.err
	}
	c.m.Unlock()
	var (
		bs  []byte
		err error
	)
	if r, ok := req.(string); ok {
		bs = []byte(r)
	} else if req != nil {
		bs, err = json.Marshal(req)
	} else {
		bs = []byte{}
	}
	if err != nil {
		return err
	}
	m := &message{
		Header: Header{
			Magic: magic,
			Len:   int32(len(bs)),
			Type:  t,
		},
		Payload: bs,
	}
	call := call{
		typ:      m.Header.Type,
		response: resp,
		err:      make(chan error),
	}
	c.m.Lock()
	c.calls = append(c.calls, call)
	err = c.writeMessage(m)
	c.m.Unlock()
	if err != nil {
		return err
	}
	return <-call.err
}

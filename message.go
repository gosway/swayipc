package swayipc

var magic = [6]byte{'i', '3', '-', 'i', 'p', 'c'}

type MessageType uint32

const (
	runCommand      MessageType = 0
	getWorkspaces   MessageType = 1
	subscribe       MessageType = 2
	getOutputs      MessageType = 3
	getTree         MessageType = 4
	getMarks        MessageType = 5
	getBarConfig    MessageType = 6
	getVersion      MessageType = 7
	getBindingModes MessageType = 8
	getConfig       MessageType = 9
	sendTick        MessageType = 10
	getBindingState MessageType = 12
	getInputs       MessageType = 100
	getSeats        MessageType = 101
)

type Header struct {
	Magic [6]byte
	Len   int32
	Type  MessageType
}

type message struct {
	Header  Header
	Payload []byte
}

func getVersionReq() *message {
	return &message{
		Header: Header{
			Magic: magic,
			Len:   0,
			Type:  7,
		},
		Payload: []byte{},
	}
}

type Version struct {
	Major      int    `json:"major"`
	Minor      int    `json:"minor"`
	Patch      int    `json:"patch"`
	Version    string `json:"human_readable"`
	ConfigPath string `json:"loaded_config_file_name"`
}

func (c *Conn) GetVersion() (v Version, err error) {
	err = c.roundTrip(getVersion, nil, &v)
	return
}

type CommandReply struct {
	Success    bool   `json:"success"`
	ParseError bool   `json:"parse_error"`
	Error      string `json:"error"`
}

func (c *Conn) RunCommand(cmd string) (r []CommandReply, err error) {
	err = c.roundTrip(getVersion, cmd, &r)
	return
}

type Rect struct {
	X int `json:"x"`
	Y int `json:"y"`
	W int `json:"width"`
	H int `json:"height"`
}

type Workspace struct {
	Num     int    `json:"num"`
	Name    string `json:"name"`
	Visible bool   `json:"visible"`
	Focused bool   `json:"focused"`
	Urgent  bool   `json:"urgent"`
	Rect    Rect   `json:"rect"`
	Output  string `json:"output"`
}

func (c *Conn) GetWorkspaces() (ws []Workspace, err error) {
	err = c.roundTrip(getWorkspaces, nil, &ws)
	return
}

type IdleInhibitors struct {
	Appliction string `json:"application"`
	User       string `json:"user"`
}

type WindowProperties struct {
	Title         string `json:"title"`
	Class         string `json:"class"`
	Instance      string `json:"instance"`
	WindowRole    string `json:"window_role"`
	WindowType    string `json:"window_type"`
	TrasitientFor string `json:"transitient_for"`
}

type Node struct {
	Id                 int              `json:"id"`
	Name               string           `json:"name"`
	Type               string           `json:"type"`
	Border             string           `json:"border"`
	CurrentBorderWidth int              `json:"current_border_width"`
	Layout             string           `json:"layout"`
	Orientation        string           `json:"orientation"`
	Percent            float64          `json:"percent"`
	Rect               Rect             `json:"rect"`
	WindowRect         Rect             `json:"window_rect"`
	DecoRect           Rect             `json:"deco_rect"`
	Geometry           Rect             `json:"geometry"`
	Urgent             bool             `json:"urgent"`
	Sticky             bool             `json:"sticky"`
	Marks              []string         `json:"marks"`
	Focused            bool             `json:"focused"`
	Focus              []int            `json:"focus"`
	Nodes              []Node           `json:"nodes"`
	FloatingNodes      []Node           `json:"floating_nodes"`
	Representation     string           `json:"representation"`
	FullscreenMode     int              `json:"fullscreen_mode"`
	AppId              string           `json:"app_id"`
	Pid                int              `json:"pid"`
	Visible            bool             `json:"visible"`
	Shell              string           `json:"shell"`
	InhibitIdle        bool             `json:"inhibit_idle"`
	IdleInhibitors     IdleInhibitors   `json:"idle_inhibitors"`
	Window             int              `json:"window"`
	WindowProperties   WindowProperties `json:"window_properties"`
}

func (c *Conn) GetTree() (tree Node, err error) {
	err = c.roundTrip(getTree, nil, &tree)
	return
}

type OutputMode struct {
	Width   int `json:"width"`
	Height  int `json:"height"`
	Refresh int `json:"refresh"`
}

type Output struct {
	Name             string       `json:"name"`
	Make             string       `json:"make"`
	Model            string       `json:"model"`
	Serial           string       `json:"serial"`
	Active           bool         `json:"active"`
	Dpms             bool         `json:"dpms"`
	Primary          bool         `json:"primary"`
	Scale            float64      `json:"scale"`
	SubpixelHinting  string       `json:"subpixel_hinting"`
	Transform        string       `json:"transform"`
	CurrentWorkspace string       `json:"current_workspace"`
	Modes            []OutputMode `json:"modes"`
	CurrentMode      OutputMode   `json:"current_mode"`
	Rect             Rect         `json:"rect"`
}

func (c *Conn) GetOutputs() (outputs []OutputMode, err error) {
	err = c.roundTrip(getOutputs, nil, &outputs)
	return
}

type SubscribeReply struct {
	Success bool `json:"success"`
}

func (c *Conn) Subscribe(eventType ...EventType) (r SubscribeReply, err error) {
	events := make([]string, len(eventType))
	for i, t := range eventType {
		events[i] = t.String()
	}
	err = c.roundTrip(subscribe, events, &r)
	return
}
